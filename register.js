$(document).ready(function() {  
    $('form').submit(function(event){
      event.preventDefault();
    });
  
    $("#submit").click(function() {
        var email = $("#email").val();
        var userName = $("#username").val();
        var password = $("#password").val();
        
        // Checking for blank fields.
        if (email == '' || password == '' || userName == '') {
          
        } else {
            $.post("api.php", {
                    email: email,
                    username: userName,
                    password: password                   
                },
                function(result) {
                    var result = JSON.parse(result);
                    if (result.errorMessage) {
                        alert(result.errorMessage);                      
                        //do something on failed user creation
                    }
                    if (result.success) {
                        alert("Success");  
                        //do something on successful user creation
                    }
                });
        }
    });
});